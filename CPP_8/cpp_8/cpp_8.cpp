#include <iostream>
#include <string>

using namespace std;


class Animal 
{
public:
    virtual void Voice()
    {
            cout << "Animals says:"<<"\n";
    }
};
 
class Dog : public Animal 
{ 
public:
    void Voice() override
    {
        cout << "Woof!" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << "\n";
    }
};

class Bird : public Animal
{
public:
    void Voice() override
    {
        cout << "Tweet!" << "\n";
    }
};



int main()
{
    Animal* A = new Animal;
    Animal* D = new Dog;
    Animal* C = new Cat;
    Animal* B = new Bird;

    Animal* array[4];

    array[0] = new Animal;
    array[1] = new Dog;
    array[2] = new Cat;
    array[3] = new Bird;

    for (int i = 0; i < 4; i++)
    {
        array[i]->Voice();
    }

  
}

